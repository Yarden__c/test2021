import { PredictService } from './../predict.service';
import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { Student } from '../interfaces/student';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  userId:string = '';
  mathAvg:number = null;
  sit:number = null;
  paid:boolean = false;
  result:string;
  results = ['dropout', 'not dropout']
  

  onSubmit(){ 
    this.studentService.createStudent(this.userId, this.mathAvg, this.sit, this.paid, this.result);
    this.router.navigate(['/students']); 
    
  }

  onCancel(){ 
    this.mathAvg = null;
    this.sit = null;
    this.paid = false;
    this.result = null;
  }

  predict(){
    this.predictService.predict(this.mathAvg, this.sit, this.paid).subscribe(
      res=> {
          console.log(res);
          if( res > '0.5'){
            var result = 'not dropout'
          }
          else{
            var result  = 'dropout'
          }
        this.result = result
      }
        
    );
  }

  constructor(
    private studentService:StudentsService, 
    private authService:AuthService, 
    private predictService:PredictService, 
    private router:Router) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
      })
  }

}
