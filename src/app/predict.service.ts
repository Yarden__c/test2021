import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  private url = "https://s0ph6zo0i6.execute-api.us-east-1.amazonaws.com/test"

  predict(mathAvg:number, sit:number, paid:boolean):Observable<any>{
    let json = {'data':
      {"mathAvg":mathAvg, "sit":sit, "paid":paid }
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        let final:string= res.body;
        console.log(final);
        return final;
      })
    )
  }

  constructor(private http:HttpClient) { }
}
