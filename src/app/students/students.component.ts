import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  displayedColumns: string[] = ['mathAvg', 'sit', 'paid', 'result','email','delete'];
  userMail: string;
  students:Student[];
  students$: any;
  userId: string;

  deleteStudent(index){
    let i = this.students[index].id;
    this.studentsService.deleteStudent(this.userId,i);
  }

  constructor(private authService:AuthService,
    private studentsService:StudentsService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.userMail = user.email;
        this.students$ = this.studentsService.getStudents(this.userId);
        this.students$.subscribe(
          docs => {
            this.students = [];
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              student.id = document.payload.doc.id;
              this.students.push(student);
            }
          }
        )
      })
      
}

}
