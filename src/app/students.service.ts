import { Student } from './interfaces/student';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  createStudent(userId:string, mathAvg:number, sit:number, paid:boolean, result:string){
    const student = {mathAvg:mathAvg, sit:sit, paid:paid, result:result  }
    this.userCollection.doc(userId).collection('students').add(student);

  }
  deleteStudent(userId:string, id:string){
    this.db.doc(`users/${userId}/students/${id}`).delete();
  }
  public getStudents(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`);
    return this.studentCollection.snapshotChanges().pipe(map(
      collection => collection.map(
        document => {
          const data =  document;
          return data;
        })))
  }

  constructor(private db:AngularFirestore) { }
}
