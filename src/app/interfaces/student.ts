export interface Student {
    id:string,
    mathAvg:number,
    sit:number,
    paid:boolean,
    result:string
}
