// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyALPMfDkzP2iYB64n80f6VRMvRfCcOIbi4",
    authDomain: "test2021-df422.firebaseapp.com",
    projectId: "test2021-df422",
    storageBucket: "test2021-df422.appspot.com",
    messagingSenderId: "301223541024",
    appId: "1:301223541024:web:8527f236aa08541fd9ff45"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
